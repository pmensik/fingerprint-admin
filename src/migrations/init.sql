-- Creates emails table
CREATE TABLE IF NOT EXISTS fingerprint
(id SERIAL PRIMARY KEY,
 user_id INTEGER NOT NULL,
 first_name VARCHAR(100) NOT NULL,
 surname VARCHAR(500) NOT NULL,
 print_marker VARCHAR(1000) NOT NULL);
