(ns fingerprint.session-manager
  (:require [cronj.core :refer [cronj]]
            [noir.session :refer [clear-expired-sessions]]))

(def cleanup-job
  (cronj
   :entries
   [{:id "session-cleanup"
     :handler (fn [_ _] (clear-expired-sessions))
     :schedule "* /30 * * * * *"
     :opts {}}]))
