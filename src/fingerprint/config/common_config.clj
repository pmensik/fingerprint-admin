(ns fingerprint.config.common-config
  (:require [environ.core :refer [env]]
            [korma.db :refer [postgres]]))

(def config {})

(def db-config (postgres {:db (env :db-name)
                          :user (env :db-user)
                          :password (env :db-password)
                          :host (env :db-host)}))
