(ns fingerprint.config.swagger-config)

(def swagger-config
  {:ui "/doc"
   :spec "/swagger.json"
   :data {:info {:title "Fingerprint admin"
                 :version "0.1.0"
                 :description "IS for management of employees fingerprints"
                 :tags [{"Fingerprint" "API for registering and getting employees fingerprints"}]}}})
