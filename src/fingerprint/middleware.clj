(ns fingerprint.middleware
  (:require [environ.core :refer [env]]
            [noir-exception.core :refer [wrap-internal-error]]
            [prone.middleware :refer [wrap-exceptions]]
            [taoensso.timbre :as timbre]))

(defn log-request [handler]
  (fn [req]
    (timbre/debug req)
    (handler req)))

(def development-middleware
  [wrap-exceptions])

(def production-middleware
  [#(wrap-internal-error % :log (fn [e] (timbre/error e)))])

(defn load-middleware []
  (concat (when (env :dev) development-middleware)
          production-middleware))
