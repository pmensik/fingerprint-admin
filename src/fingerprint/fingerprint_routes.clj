(ns fingerprint.fingerprint-routes
  (:require [compojure.api.sweet :refer :all]
            [fingerprint.db.print-entity :as db]
            [fingerprint.schema.fingerprint :refer [Fingerprint]]
            [ring.util.http-response :refer [created ok]]
            [schema.core :as s]
            [taoensso.timbre :as timbre]))

(defn- mock-abra-api
  [print-record]
  (assoc print-record :first-name "name" :surname "surname"))

(defn register-print
  [print-record]
  (let [updated-record (mock-abra-api print-record)]
    (db/register-print updated-record)
    (timbre/info "Saved new fingerprint record to the DB")
    (created "" {:message "New fingerprint has been saved"})))
;; TODO fix url

(defn- download-all-prints
  []
  (timbre/info "Getting all available fingerprint records")
  (ok (into [] (db/get-all-prints))))

(def print-routes
  (context "/fingerprint/api" []
    :tags ["Fingerprint"]

    (POST "/register" {:as request}
      :body       [print-record Fingerprint]
      :responses {201 {:schema {:message s/Str}, :description "Returns created message"}}
      :summary "Registers new fingerprint."
      (register-print print-record))

    (GET "/prints" {:as request}
      :responses {200 {:description "Returns all saved fingerprints"}}
      :summary    "Downloads all available prints with their corresponding employee data."
      (download-all-prints))))
