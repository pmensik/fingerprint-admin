(ns fingerprint.handler
  (:require [compojure.api.sweet :refer [defapi middleware]]
            [cronj.core :as cronj]
            [environ.core :refer [env]]
            [fingerprint.config.common-config :refer [db-config]]
            [fingerprint.config.swagger-config :refer [swagger-config]]
            [fingerprint.fingerprint-routes :refer [print-routes]]
            [fingerprint.session-manager :as session-manager]
            [migratus.core :as migratus]
            [noir.response :refer [redirect]]
            [ring.middleware.defaults :refer [site-defaults]]
            [taoensso.timbre :as timbre]))

(def migratus-config
  {:store :database
   :migration-dir "migrations"
   :init-script "init.sql"
   :db db-config})

(defn- migrate-db
  []
  (timbre/info "Checking migrations")
  (try
    (migratus/init migratus-config)
    (migratus/migrate migratus-config)
    (catch Exception e (timbre/error "Failed to migrate DB" e)))
  (timbre/info "Finished migration"))

(defn init
  "init will be called once when
   app is deployed as a servlet on
   an app server such as Tomcat
   put any initialization code here"
  []
  ;; add logging configuration

  (migrate-db)
  (cronj/start! session-manager/cleanup-job)
  (timbre/info "\n-=[ Fingerprint Admin started successfully"
               (when (:dev env) "using the development profile") "]=-"))

(defn destroy
  "destroy will be called when your application
   shuts down, put any clean up code here"
  []
  (timbre/info "Fingerprint Admin is shutting down...")
  (cronj/shutdown! session-manager/cleanup-job)
  (timbre/info "shutdown complete!"))

;; timeout sessions after 30 minutes
(def session-defaults
  {:timeout (* 60 30)
   :timeout-response (redirect "/")})

(defn- mk-defaults
  "set to true to enable XSS protection"
  [xss-protection?]
  (-> site-defaults
      (update-in [:session] merge session-defaults)
      (assoc-in [:security :anti-forgery] xss-protection?)))

(defapi app {:format [:json-kw :transit-json]
             :swagger swagger-config}
  (middleware [mk-defaults])
  print-routes)
