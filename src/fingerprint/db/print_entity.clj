(ns fingerprint.db.print-entity
  (:require [fingerprint.config.common-config :refer [db-config]]
            [korma.core :refer :all]))

(defentity print-entity
  (table :fingerprint)
  (database db-config))

(defn register-print
  "Saves fingerprint and corresponding employee to the DB"
  [print-record]
  (insert print-entity
          (values {:user_id (:employee-id print-record)
                   :first_name (:first-name print-record)
                   :surname (:surname print-record)
                   :print_marker (:print-marker print-record)})))

(defn get-all-prints
  "Gets all recorded prints"
  []
  (select print-entity))
