(ns fingerprint.test-fingerprint-routes
  (:require [cheshire.core :as json]
            [fingerprint.handler :refer [app]]
            [midje.sweet :refer :all]
            [peridot.core :refer :all]))

(defn- create-register-request
  [body]
  (-> (session app)
      (request "/fingerprint/api/register"
               :request-method :post
               :content-type "application/json"
               :body body)
      (:response)))

(defn- create-download-request
  []
  (-> (session app)
      (header "Accept" "application/json")
      (request "/fingerprint/api/prints"
               :request-method :get
               :content-type "application/json")
      (:response)))

(defn- get-parsed-body
  [response]
  (-> :body response slurp (json/parse-string true)))

(facts "Test register fingerprint route with incorrect print record will return bad request"
       (fact "Missing employee id should result in bad request"
             (let [print-request (json/generate-string {:print-marker "abcd"})
                   response (create-register-request print-request)]
               response => (contains [[:status 400]])
               ((get-parsed-body response) :errors) => {:employee-id "missing-required-key"}))

       (fact "Empty fingerprint marker should result in bad request"
             (let [print-request (json/generate-string {:print-marker "" :employee-id 1})
                   response (create-register-request print-request)]
               response => (contains [[:status 400]])
               ((get-parsed-body response) :errors) => (contains {:print-marker anything}))))

; (fact "Registering new fingerprint should correcly return 201"
;   (let [body (json/generate-string {:employee-id 1 :print-marker "aaa"})
;         response (create-register-request body)]
;     response => (contains [[:status 201]])
;       (provided (fingerprint.db.print-entity/register-print body) => nil)))

; (fact "Download all should return ok response and list of prints"
;   (let [response (create-download-request)]
;     (println (get-parsed-body response))
;     response => (contains [[:status 200]])
;     (count (get-parsed-body response)) => 20))
