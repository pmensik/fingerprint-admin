(defproject
  fingerprint-admin
  "0.1.0-SNAPSHOT"
  :description
  "Information system for fingerprint company"
  :url
  "http://example.com/FIXME"
  :min-lein-version "2.0.0"
  :dependencies
   [[org.clojure/clojure "1.8.0"]
   [prone "1.1.4"]
   [pandect "0.6.0"]
   [noir-exception "0.2.5"]
   [com.taoensso/timbre "4.7.4"]
   [org.postgresql/postgresql "9.3-1102-jdbc41"]
   [lib-noir "0.9.9"]
   [environ "1.1.0"]
   [ring-server "0.4.0"]
   [im.chit/cronj "1.4.4"]
   [compojure "1.5.1"]
   [metosin/compojure-api "1.2.0-alpha1"]
   [clj-http "3.4.1"]
   [korma "0.4.3"]
   [migratus "0.8.32"] 
   [clj-time "0.12.2"] ; needed by compojure-api
   [com.novemberain/validateur "2.5.0"]
   [slingshot "0.12.2"]
   [cheshire "5.6.3"]]
  :repl-options
  {:init-ns fingerprint.repl}
  :jvm-opts
  ["-server"]
  :plugins
  [[lein-ring "0.8.13"]
   [lein-environ "1.0.0"]
   [lein-ancient "0.5.5"]]
  :ring
    {:handler fingerprint.handler/app,
     :init fingerprint.handler/init,
     :destroy fingerprint.handler/destroy
     :uberwar-name "flexiana-mailer.war"}
  :profiles
    {:uberjar {:omit-source true
               :aot :all}
     :uberwar {:omit-source true
               :aot :all}
     :dev {:dependencies [[midje "1.6.3"]
                          [peridot "0.4.4"]]
           :ring {:open-browser? false}
           :plugins [[lein-midje "3.2.1"]]
           :env {:db-user "print_admin"
                 :db-password "print_admin"
                 :db-host "localhost"
                 :db-name "print_admin"}}})
