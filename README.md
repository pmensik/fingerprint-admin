# Fingerprint Admin System

## Prerequisites

Created database with a user on the target host. Application is currently running on Postgres so it can be created with
following commands.

```
CREATE DATABASE print_admin;
CREATE USER print_admin WITH PASSWORD 'print_admin';
GRANT ALL PRIVILEGES ON DATABASE print_admin TO print_admin;
```

Application can obtain DB credentials and host from environment or Java variables or it will fallback to defaults
(everything is `print_admin`) then.

## Documentation

Documentation is automatically generated via Swagger so all you need is to run `lein ring server` and enjoy docs  on
`<your-host>:3000/doc/index.html`

## Running

To start a web server for the application, run:

    lein ring server

